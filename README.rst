This is a repository collecting `hass.io add-ons`_ developed by chrysn_.

To add it into your hass.io installation, go to its dashboard's add-on store
page and add this URL as a new repository:

https://gitlab.com/chrysn/hassio-addons

.. _`hass.io add-ons`: https://www.home-assistant.io/getting-started/configuration/#installing-hassio-add-ons
.. _chrysn: chrysn@fsfe.org
